module.exports = function(wallaby) {
  // change this expression to reflect your naming conventions
  const testPathExp = 'src/**/*test.ts?(x)';

  return {
    files: [
      'tsconfig.json',
      'tsconfig.test.json',
      './src/setupTests.js',
      'src/**/*.+(js|jsx|ts|tsx|json|snap|css|less|sass|scss|jpg|jpeg|gif|png|svg)',
      `!${testPathExp}`,
    ],

    tests: [testPathExp],

    env: {
      type: 'node'
    },

    compilers: {
      '**/*.js?(x)': wallaby.compilers.babel({
        babel: require('babel-core'),
        presets: ['react-app'],
      }),
      '**/*.ts?(x)': wallaby.compilers.typeScript({
        module: 'commonjs',
        jsx: 'React',
      }),
    },

    testFramework: 'jest',
    setup: (wallaby) => {
      wallaby.testFramework.configure({
        setupFiles: ["./src/setupTests.js"]
      });
    }
  };
};
