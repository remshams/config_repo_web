const jsdom = require("jsdom").JSDOM;
const debug = require("debug")("build:configs:tshook");
const trace = require("debug")("build:configs:tshook:trace");
const sinon = require("sinon");

require("ts-node").register({
  project: "./tsconfig.test.json",
});

require("ignore-styles");

// initialize window project
const {window} = new jsdom("<!doctype html><html><body></body></html>", {
    url: "http://localhost",
    href: "http://localhost",
    referrer: "http://localhost",
    userAgent: "node.js",
    includeNodeLocations: true,
    language: "en"
});

//
// provide requestAnimationFrame and cancelAnimationFrame
const vendors = ['ms', 'moz', 'webkit', 'o'];
let lastTime = 0;
for (let x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
}

if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback, element) {
        let currTime = Date.now();
        let timeToCall = Math.max(0, 16 - (currTime - lastTime));
        let id = window.setTimeout(function () {
            callback(currTime + timeToCall);
        }, timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };
}

if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
        clearTimeout(id);
    };
}

window.XMLHttpRequest = sinon.useFakeXMLHttpRequest();

//
//

function copyProps(src, target) {
    const props = Object.getOwnPropertyNames(src)
        .filter(prop => typeof target[prop] === "undefined")
        .map(prop => {
            trace("prop : %o", prop);
            return Object.getOwnPropertyDescriptor(src, prop);
        });
    trace("props : %o", props);
    Object.defineProperties(target, props);
}

copyProps(window, global);
// adding missing references for window
window.Object = Object;
window.Math = Math;
window.Promise = Promise;

global.window = window;
global.document = window.document;
global.HTMLElement = window.HTMLElement;
global.navigator = {
    userAgent: "node.js",
    language: "en"
};
global.XMLHttpRequest = window.XMLHttpRequest;
global.requestAnimationFrame = window.requestAnimationFrame;

debug("Initialization for global object is finished...");
