import * as React from "react";
import { InjectedI18nProps, InjectedTranslateProps } from "react-i18next/src/props";
import withi18n from "./_shared/hocs/withi18n";
import withTheme from "./_shared/hocs/withTheme";
import PageHeader from "./_shared/pageHeader/components/page_header";
import Route, { PathParamsProp } from "./_shared/routing/components/route";
import LazyRoute from "./_shared/routing/components/lazy_route";
import HomeComp from "./home/components/home";
import Router from "./_shared/routing/components/router";

type AppProps = InjectedTranslateProps & InjectedI18nProps;


const Environment = (params: PathParamsProp) => (
  <>
    <div>Environments</div>
    <div>{params.params["environmentId"]}</div>
  </>
);


const Project = (params: PathParamsProp) => (
  <>
    <div>Sub</div>
    <div>{params.params["projectId"]}</div>
    <Route path="environments/:environmentId" Component={Environment}/>
  </>
);

const Home = () => {
  const applicationLoader = () => import("./application/components/application");
  return (
    <>
      <HomeComp/>
      <Route path="projects/:projectId" Component={Project}/>
      <LazyRoute path={"application"} loader={applicationLoader}/>
    </>
  );

}

const Main = () => (
  <Route Component={Home}/>
);


const App: React.StatelessComponent<AppProps> = () => {

  return (
    <>
      <PageHeader/>
      <Router>
        <Main/>
      </Router>
    </>
  );
};

const AppWithHocs = withi18n(withTheme(App));

export default AppWithHocs;
