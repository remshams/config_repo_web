import * as React from "react";
import List from "material-ui/List";
import ListItem from "material-ui/List/ListItem";
import ListItemText from "material-ui/List/ListItemText"
import Button from "material-ui/Button";
import { translate } from "react-i18next";

const Application = () => (
  <>
    <List>
      <ListItem>
        <ListItemText>First</ListItemText>
      </ListItem>
      <ListItem>
        Second
      </ListItem>
    </List>
    <Button color="secondary">Test</Button>
  </>
);

export default translate("common")(Application);
