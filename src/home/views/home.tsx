import * as React from "react";
import Link from "../../_shared/routing/components/link";


const Home = () => (
  <>
    <Link path="/projects">
      <div>Home</div>
    </Link>
    <Link path="/application">
      <div>Application</div>
    </Link>
  </>
);

export default Home;
