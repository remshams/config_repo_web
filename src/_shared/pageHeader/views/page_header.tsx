import * as React from "react";
import { AppBar, Toolbar, Typography } from "material-ui"
import { translate } from "react-i18next";
import { InjectedTranslateProps } from "react-i18next/src/props";

type PageHeaderProps = InjectedTranslateProps

const PageHeader = ({ t }: PageHeaderProps) => (
  <div>
    <AppBar position="static" color="primary">
      <Toolbar>
        <Typography variant="title" color="inherit">
          {t("title")}
        </Typography>
      </Toolbar>
    </AppBar>
  </div>
);

export default translate("common")(PageHeader);
