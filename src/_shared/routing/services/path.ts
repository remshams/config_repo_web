import { fromNullable, none, Option, some } from "fp-ts/lib/Option";
import { equals, isEmpty, not, partial, pipe, reject, split } from "ramda";
import { doesPathElementMatch } from "./path_params";

// const routeMap = {
//   application: Loadable({
//     loader: () => import(/* webpackChunkName: "application" */ "../../../application/components/application"),
//     modules: ["application"],
//     loading: Loading,
//   }),
//   home: Loadable({
//     loader: () => import(/* webpackChunkName: "home" */ "../../../home/components/home"),
//     modules: ["home"],
//     loading: Loading,
//   })
// };

export type RouteElement = string;
export type PathParams = {
  [key: string]: any
}


export const extractPathElements = (path?: string) =>
  fromNullable(path)
    .map(partial(split, ["/"]))
    .map(partial<((element: string) => boolean), Array<string>, Array<string>>(reject, [isEmpty]))
    .getOrElse([]);

export const stripPathElementsOnRouteContext = (contextElements: Array<RouteElement>, routeElements: Array<RouteElement>): Array<RouteElement> | null =>
  some(routeElements)
    .chain(() => findFirstRouteElementMatch(contextElements, routeElements))
    .map(sliceRouteElementsFrom(routeElements))
    .toNullable();

export const extractPathElementsOnRouteContext = (context: string, path: string): Array<RouteElement> | null =>
  some(path)
    .chain(() => fromNullable({ routeContext: extractPathElements(context), routeElements: extractPathElements(path) }))
    .map(({ routeContext, routeElements }) => stripPathElementsOnRouteContext(routeContext, routeElements))
    .toNullable();


const findFirstRouteElementMatch = (contextElements: Array<RouteElement>, routeElements: Array<RouteElement>): Option<number> =>
  some(contextElements)
    .map(() => contextElements.filter(pipe(isEmpty, not)))
    .filter(pipe(isEmpty, not))
    .chain(nonEmptyRouteContext => findIndexOptional(nonEmptyRouteContext[0], routeElements));

const findIndexOptional = <Element>(match: Element, source: Array<Element>): Option<number> => {
  const index = source.findIndex(equals(match));
  return index > -1 ?
    some(index):
    none;
};

const sliceRouteElementsFrom = (routeElements: Array<RouteElement>) => (startIndex: number) => routeElements.slice(startIndex, routeElements.length);

export const areContextElementsInPath = (contextElements: Array<RouteElement>, pathElements: Array<RouteElement>) =>
  pipe(
    ({ routeElements, pathElements }) => ({ routeElements,  pathElements: stripPathElementsOnRouteContext(routeElements, pathElements)}),
    ({ routeElements, pathElements }) => doPathElementsMatch(routeElements, fromNullable(pathElements).getOrElse([]))
  )({ routeElements: contextElements, pathElements });

const doPathElementsMatch = (contextElements: Array<RouteElement>, pathElements: Array<RouteElement>) =>
  calculatePathElementsLengthWithParamOffset(pathElements.length) >= contextElements.length ?
    contextElements.reduce((doesMatch, contextElement, index) => doesMatch && doesPathElementMatch(contextElement, pathElements[index]), true) :
    false;

const calculatePathElementsLengthWithParamOffset = (pathElementsLength: number) => pathElementsLength + 1;
