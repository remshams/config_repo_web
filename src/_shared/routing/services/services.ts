import { fromEvent } from "rxjs/internal/observable/fromEvent";
import { map } from "rxjs/operators";
import { RouterServices } from "../interfaces/services";

const getCurrentPath = (window: Window) => () => window.location.pathname;
const pathObservable = (window: Window) => fromEvent(window, "popstate")
  .pipe(
    map(event => getCurrentPath(window)())
  );
const navigateToPath = (window: Window) => (path: string) => {
  window.history.pushState({currentPage: path} , path, path);
  window.dispatchEvent(new PopStateEvent("popstate"));
};

export const createRouterServices = (windowObject = window): RouterServices => ({
  path: {
    getCurrentPath: getCurrentPath(windowObject),
    pathObservable: pathObservable(windowObject)
  },
  navigation: {
    navigateToPath: navigateToPath(windowObject)
  }
});

export const routerServices = createRouterServices();
