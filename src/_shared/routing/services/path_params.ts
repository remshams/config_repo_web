import { PathParams, RouteElement, stripPathElementsOnRouteContext } from "./path";
import { fromNullable } from "fp-ts/lib/Option";

export const extractPathParamsOnRouteContext = (contextElements: Array<RouteElement>, routeElements: Array<RouteElement>): PathParams =>

  fromNullable(stripPathElementsOnRouteContext(contextElements, routeElements))
    .filter((strippedElements) => areRouteElementsValid(contextElements, strippedElements))
    .map(strippedElements => trimToLongest(contextElements, strippedElements))
    .map(({ trimmedRouteContexts, trimmedRouteElements }) => extractPathParamNames(trimmedRouteContexts, trimmedRouteElements))
    .getOrElse({});

const trimToLongest = (contextElements: Array<RouteElement>, routeElements: Array<RouteElement>): { trimmedRouteContexts: Array<RouteElement>, trimmedRouteElements: Array<RouteElement> } =>
  contextElements.length > routeElements.length ?
    { trimmedRouteContexts: contextElements.slice(0, routeElements.length), trimmedRouteElements: routeElements } :
    { trimmedRouteContexts: contextElements, trimmedRouteElements: routeElements.slice(0, contextElements.length) };

const areRouteElementsValid = (contextElements: Array<RouteElement>, routeElements: Array<RouteElement>): boolean =>
  routeElements.reduce((isValid, pathElement, index) => isValid && isRoutePathElementValid(pathElement, contextElements[index]), true);

const isRoutePathElementValid = (pathElement: RouteElement, contextElement: RouteElement) =>
  fromNullable(pathElement)
    .chain(() => fromNullable(contextElement).map(() => ({ validPathElement: pathElement, validContextElement: contextElement })))
    .map(({ validPathElement, validContextElement }) =>
      doesPathElementMatch(validContextElement, validPathElement)
    ).getOrElse(true);


const extractPathParamNames = (contextElements: Array<RouteElement>, routeElements: Array<RouteElement>): PathParams =>
  contextElements
    .reduce((pathParams: PathParams, currentContext, index) =>
      ({...pathParams, ...extractParam(currentContext, routeElements[index])}), {});

const extractParam = (contextElement: RouteElement, pathElement: RouteElement | undefined) =>
  canExtractParam(contextElement, pathElement) ?
    {
      [makeParamName(contextElement)]: pathElement
    } :
    {};

const makeParamName = (paramName: string) => paramName.replace(":", "");
const canExtractParam = (contextElement: RouteElement, pathElement: RouteElement | undefined) => pathElement !== undefined && isPlaceHolder(contextElement);
export const isPlaceHolder = (routeElement: RouteElement) => routeElement.startsWith(":");
export const doesPathElementMatch = (contextElement: RouteElement, pathElement: RouteElement) =>
  isPlaceHolder(contextElement) ?
    true :
    contextElement === pathElement;
