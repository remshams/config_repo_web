import { doesPathElementMatch, extractPathParamsOnRouteContext, isPlaceHolder } from "../path_params";

describe("extractPathParamsOnRouteContext", () => {

  it("extracts single param", () => {

    const context = ["parent", ":parentId", "child"];

    const path = ["parent", "1", "child", "other"];

    const extractedPathParamsExpected = {
      parentId: "1",
    };

    expect(extractPathParamsOnRouteContext(context, path)).toEqual(extractedPathParamsExpected);


  });

  it("extracts multiple params", () => {

    const context = ["parent", ":parentId", "child", ":childId"];

    const path = ["parent", "1", "child", "2"];

    const extractedPathParamsExpected = {
      parentId: "1",
      childId: "2"
    };

    expect(extractPathParamsOnRouteContext(context, path)).toEqual(extractedPathParamsExpected);

  });

  it("extracts params in case path is longer", () => {

    const context = ["parent", ":parentId", "child", ":childId"];

    const path = ["parent", "1", "child", "2", "anotherChild", ":anotherChildId"];

    const extractedPathParamsExpected = {
      parentId: "1",
      childId: "2"
    };

    expect(extractPathParamsOnRouteContext(context, path)).toEqual(extractedPathParamsExpected);

  });

  it("extracts params in case context is longer", () => {

    const context = ["parent", ":parentId", "child", ":childId"];

    const path = ["parent", "1"];

    const extractedPathParamsExpected = {
      parentId: "1",
    };

    expect(extractPathParamsOnRouteContext(context, path)).toEqual(extractedPathParamsExpected);

  });

  it("extracts params in case context does not start from the beginning", () => {

    const context = ["child", ":childId"];

    const path = ["parent", "1", "child", "2"];

    const extractedPathParamsExpected = {
      childId: "2",
    };

    expect(extractPathParamsOnRouteContext(context, path)).toEqual(extractedPathParamsExpected);

  });

  it("returns empty object in case there is nothing to extract", () => {

    const context = ["parent", "child"];

    const path = ["parent", "1", "child", "2"];

    const extractedPathParamsExpected = {};

    expect(extractPathParamsOnRouteContext(context, path)).toEqual(extractedPathParamsExpected);

  });

  it("ignores elements exceeding context elements", () => {

    const context = ["parent", ":parentId"];

    const path = ["parent", "1", "child", "2"];

    const extractedPathParamsExpected = {
      parentId: "1"
    };

    expect(extractPathParamsOnRouteContext(context, path)).toEqual(extractedPathParamsExpected);

  });

  it("returns empty object in case structures do not match", () => {

    const context = ["parent", ":parentId"];

    const path = ["anotherParent", "1"];

    const extractedPathParamsExpected = {};

    expect(extractPathParamsOnRouteContext(context, path)).toEqual(extractedPathParamsExpected);

  });

  it("returns empty object in case path and context do not path", () => {

    const context = ["parent", ":parentId"];

    const path = ["child", "1"];

    const extractedPathParamsExpected = {};

    expect(extractPathParamsOnRouteContext(context, path)).toEqual(extractedPathParamsExpected);

  });

  describe("isPlaceHolder", () => {

    it("returns true in case element contains :", () => {

      expect(isPlaceHolder(":test")).toEqual(true);

    });

    it("returns false in case element does not contain :", () => {

      expect(isPlaceHolder("test")).toEqual(false);

    });

  });

  describe("doesPathElementMatch", () => {

    it("returns true in case normal elements matches", () => {

      const element = "test";

      expect(doesPathElementMatch(element, element)).toEqual(true);

    });

    it("returns true in case placeholder element matches", () => {

      const element = ":test";

      expect(doesPathElementMatch(element, "1")).toEqual(true);

    });

    it("returns false in case normal element does not match", () => {

      const element = "test";

      expect(doesPathElementMatch(element, "1")).toEqual(false);

    });

  });

});
