import { addPathDefinition } from "../router_context";

describe("Router Context", () => {

  describe("Router actions", () => {

    describe("addPathDefinition", () => {


      it("appends path to empty path definition", () => {

        const pathDefinition = "parent/:parentId/child/:childId";

        const pathDefinitionExpected = ["parent", ":parentId", "child", ":childId"];

        expect(addPathDefinition([], pathDefinition)).toEqual(pathDefinitionExpected);

      });


      it("appends path to existing path definition", () => {

        const pathDefinition = "parent/:parentId";
        const addedPathDefinition = "child/:childId";

        const pathDefinitionReturned = addPathDefinition(addPathDefinition([], pathDefinition), addedPathDefinition);

        const pathDefinitionExpected = ["parent", ":parentId", "child", ":childId"];

        expect(pathDefinitionReturned).toEqual(pathDefinitionExpected);


      });

    });

  });


});

