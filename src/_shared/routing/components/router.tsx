import * as React from "react";
import { createDefaultRouterContext, RouterContext } from "./router_context";
import { extractPathElements } from "../services/path";

type RouterProps = {
  routerContext?: RouterContext,
}

type RouterState = {
  routerContext: RouterContext
}

class Router extends React.Component<RouterProps, RouterState> {

  constructor(props: RouterProps) {
    super(props);
    this.initState();
  }

  public componentDidMount() {
    this.registerPathChange();
  }


  public render() {
    return (
      <RouterContext.Provider value={this.state.routerContext}>
        {this.props.children}
      </RouterContext.Provider>
    )
  }

  private initState() {
    let routerContext = this.props.routerContext || createDefaultRouterContext();
    routerContext = this.updateRouterContextOnPathChange(routerContext, routerContext.services.path.getCurrentPath());
    this.state = {
      routerContext
    };
  }

  private registerPathChange() {
    this.state.routerContext.services.path.pathObservable.subscribe(this.onPathChange.bind(this));
  }

  private updateRouterContextOnPathChange(prevContext: RouterContext, path: string) {
    const newPathElements = extractPathElements(path);
    return {
        ...prevContext,
        router: {
          ...prevContext.router,
          data: {
            ...prevContext.router.data,
            currentPath: path,
            pathElements: newPathElements
          }
        }
      }
  }

  private onPathChange(path: string) {
    this.setState({
      ...this.state,
      routerContext: this.updateRouterContextOnPathChange(this.state.routerContext, path)
      });
  }


}

export default Router;
