import * as React from "react";

const LazyComponent = () => (
  <h1>I am lazy</h1>
);

export default LazyComponent;
