import * as React from "react";
import * as Loadable from "react-loadable";
import Route, { PathParamsProp } from "./route";
import Loading from "./loading";

export type LazyRouteProps = {
  path?: string;
  loader: any
}

const prepareComponent = (loader: any) => {
  return Loadable<PathParamsProp, {}>({
    loader,
    loading: Loading
  });
};


const LazyRoute = (props: LazyRouteProps) => (
  <Route Component={prepareComponent(props.loader)} path={props.path}/>
);

export default LazyRoute;
