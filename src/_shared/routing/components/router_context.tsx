import * as React from "react";
import { RouterServices } from "../interfaces/services";
import { routerServices } from "../services/services";
import { extractPathElements, RouteElement } from "../services/path";

export type RouterContext = {
  services: RouterServices,
  router: {
    data: {
      currentPath: string,
      pathElements: Array<RouteElement>,
    }
  }
};

export type InjectedRouterContextProps = {
  routerContext: RouterContext
}


export const addPathDefinition = (previousDefinition: Array<RouteElement>, newPathDefinition: string): Array<RouteElement> =>
  [...previousDefinition, ...extractPathElements(newPathDefinition)];

export const createDefaultRouterContext = (): RouterContext => ({
  services: routerServices,
  router: {
    data: {
      currentPath: "",
      pathElements: [],
    }
  }
});


export const RouterContext = React.createContext(createDefaultRouterContext());
