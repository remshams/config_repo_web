import * as React from "react";
import { createDefaultRouterContext, RouterContext } from "../router_context";

const withRouterContext = (context = createDefaultRouterContext()) => <P extends any>(Component: React.ComponentType<P>) => (props: P) => (
  <RouterContext.Provider value={context}>
    <Component {...props} />
  </RouterContext.Provider>
);

export default withRouterContext;
