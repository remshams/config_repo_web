import * as React from "react";
import { InjectedRouterContextProps, RouterContext } from "../router_context";
import { Omit } from "type-zoo";

const injectRouterContext = <P extends any>(Component: React.ComponentType<P>) => (props: Omit<P, keyof InjectedRouterContextProps>) => (
  <RouterContext.Consumer>
    {routerContext => <Component {...props} routerContext={routerContext} />}
  </RouterContext.Consumer>
);

export default injectRouterContext;
