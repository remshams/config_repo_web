import * as React from "react";
import { ReactNode } from "react";
import { InjectedRouterContextProps, RouterContext } from "./router_context";
import injectRouterContext from "./hocs/injectRouterContext";

export type LinkProps = {
  path: string,
  children?: ReactNode
} & InjectedRouterContextProps

const createNavigateTo = (routerContext: RouterContext, path: string) => (event: React.MouseEvent<HTMLElement>) => {
  event.preventDefault();
  routerContext.services.navigation.navigateToPath(path);
};

const Link: React.StatelessComponent<LinkProps> = (props: LinkProps) => {
  const navigateTo = createNavigateTo(props.routerContext, props.path);
  return (
    <a href={props.path} onClick={navigateTo}>{props.children}</a>
  );
};

export default injectRouterContext(Link);
