import * as React from "react";
import i18n from "../i18n/i18n";
import { I18nextProvider } from "react-i18next";

const withi18n = <P extends any>(Component: React.ComponentType<P>) => (props: object) => (
  <I18nextProvider i18n={i18n}>
    <Component {...props} />
  </I18nextProvider>
);

export default withi18n;
