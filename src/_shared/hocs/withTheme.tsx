import * as React from "react";
import createMuiTheme from "material-ui/styles/createMuiTheme";
import { grey, red } from "material-ui/colors";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: red[400],
      main: red[700],
      dark: red[900]
    },
    secondary: {
      light: grey[400],
      main: grey[700],
      dark: grey[900]
    },
    background: {
      default: grey[200]
    }

  }
});


const withTheme = <P extends any>(Component: React.ComponentType<P>) => (props: object) => (
  <MuiThemeProvider theme={theme}>
    <Component {...props} />
  </MuiThemeProvider>
);

export default withTheme;
