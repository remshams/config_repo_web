import * as i18n from "i18next";
import * as LanguageDetector from "i18next-browser-languagedetector";
import { reactI18nextModule } from "react-i18next";
import * as Backend from "i18next-xhr-backend";

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(reactI18nextModule)
  .init({
    fallbackLng: "en",
    ns: "common",
    defaultNS: "common",
    debug: true,
    interpolation: {
      escapeValue: false
    },
    react: {
      wait: false
    },
    backend: {
      loadPath: "/locales/{{lng}}/{{ns}}.json"
    }

  });

export default i18n;

